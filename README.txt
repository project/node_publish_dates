This module simply provide 2 new property to nodes.

For more information about this module, visit the project page at https://drupal.org/project/node_publish_dates

Development of the module is sponsored by JOBaProximite (http://www.jobaproximite.com/).
